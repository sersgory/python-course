from decimal import *
cathetus1 = input("Enter 1st cathetus value: ")
cathetus2 = input("Enter 2nd cathetus value: ")
# program fails on non-numerical input
# convert input values to float and calculate hypotenuse
hypotenuse = (float(cathetus1)**2 + float(cathetus2)**2)**0.5 
# set output rounded by 3 digits after comma
hypotenuse = round(hypotenuse,3) 
print(f"Hypotenuse approx. equals to: {hypotenuse}")