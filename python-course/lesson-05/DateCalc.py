from datetime import datetime, timedelta
import sys
# Not take into account differenses in months lengh and leap year
print("usage: + or - count period for example \"+ 7 d\" will add 7 days to\
 current date ")
now = datetime.now()
try:
    subtractoradd = sys.argv[1]
    enteredperiod = sys.argv[2]
    periodsign = sys.argv[3]
    if periodsign == "h":
        if subtractoradd == "+":
            deltaDays = datetime.now() + timedelta(hours=int(enteredperiod))
            print(enteredperiod, "hours ahead date is:",\
                 deltaDays.strftime('%Y-%m-%d'))
        elif subtractoradd == "-":
            deltaDays = datetime.now() - timedelta(hours=int(enteredperiod))
            print(enteredperiod, "hours ago date was:",\
                 deltaDays.strftime('%Y-%m-%d'))
        else:
            print()
    elif periodsign == "d":
        if subtractoradd == "+":
            deltaDays = datetime.now() + timedelta(days=int(enteredperiod))
            print(enteredperiod, "days ahead date is:",\
                 deltaDays.strftime('%Y-%m-%d'))
        elif subtractoradd == "-":
            deltaDays = datetime.now() - timedelta(days=int(enteredperiod))
            print(enteredperiod, "days ago date was:",\
                 deltaDays.strftime('%Y-%m-%d'))
        else:
            print()
    elif periodsign == "m":
        if subtractoradd == "+":
            deltaDays = datetime.now() + timedelta(days=int(enteredperiod) * 30)
            print(enteredperiod, "months ahead date is:",\
                 deltaDays.strftime('%Y-%m-%d'))
        elif subtractoradd == "-":
            deltaDays = datetime.now() - timedelta(days=int(enteredperiod) * 30)
            print(enteredperiod, "months ago date was:",\
                 deltaDays.strftime('%Y-%m-%d'))
        else:
            print()
    elif periodsign == "y":
        if subtractoradd == "+":
            deltaDays = datetime.now() + timedelta(days=int(enteredperiod)\
                 * 365)
            print(enteredperiod, "years ahead date is:",\
                 deltaDays.strftime('%Y-%m-%d'))
        elif subtractoradd == "-":
            deltaDays = datetime.now() - timedelta(days=int(enteredperiod)\
                * 365)
            print(enteredperiod, "years ago date was:",\
                 deltaDays.strftime('%Y-%m-%d'))
        else:
            print()
    else:
        print()
except:
    print("invalid parameters, rerun. Usage: + - Number for period and period\
 specification")
input()