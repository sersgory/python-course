import sys
import re
print("enter text, :quit + enter to exit program")
countwords = 0
for line in sys.stdin:
    if line == (':quit\n'):
        break
    # filter with regural expressions only characters
    splittedline = re.split('[^a-zA-Z\-]',line)
    # remove all null values like caused by enter/new line
    splittedline = list(filter(None,splittedline))
    countwords = countwords + len(splittedline) 
print(countwords)


