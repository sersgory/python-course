import os

def print_fullpath(n):
    if os.path.isfile(n[0]):
        yield n[0]
    else: 
        for x in os.listdir(n[0]):
            sub=os.path.join(n[0],x)
            if os.path.isfile(sub): yield sub
            else: 
                for subentries in print_fullpath([sub]):
                        yield subentries
entered_path = input("Enter path for listing, for example on windows\
 c:\\temp + enter or press enter for current dir listing\n: ")
if len(entered_path) == 0:
    entered_path = os.getcwd()
print("working with path: ", entered_path)
working_path = [os.path.normpath(entered_path)]
for x in print_fullpath(working_path):
    print(x)